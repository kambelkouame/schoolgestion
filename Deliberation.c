#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>

#include "gestNote.h"

void deliberationMatiere(float moy) {
// void deliberationMatiere(struct Matiere matiere) {

    if(moy < moyAdmin) {
        printf("L'etudiant reprend dans la matiere avec une moyenne de : %.2f !\n", moy);
    } else if (moy > moyAdmin) {
        printf("L'etudiant est admin dans la matiere avec une moyenne de : %.2f !\n", moy);
    }

    
}

void deliberationGenerale(){

    static char *host = "localhost";
    static char *user = "root";
    static char *pass = "*23Juillet1998";
    static char *dbname = "gestion_note";

    unsigned int port = 3306;
    static char *unix_socket = NULL;
    unsigned int flag = 0;
    

    char requete[2000] = "SELECT * FROM etudiant_note";

    float moy_francais;
    float moy_anglais;
    float moy_math;
    float moy_generale;

    int i = 0;
    int num_fields;

    MYSQL *conn;
    MYSQL_RES *res;
    MYSQL_ROW row;

    conn = mysql_init(NULL);

    if(!(mysql_real_connect(conn, host, user, pass, dbname, port, unix_socket, flag))){

        fprintf(stderr, "\nError: %s [%d]\n", mysql_error(conn), mysql_errno(conn));
        exit(1);

    }
    //printf("Connection Successful! \n\n");

    //sprintf(requete, "SELECT * FROM vehicule WHERE date_sortie_vehicule = NULL");

    mysql_query(conn, requete);

    res = mysql_store_result(conn);

    num_fields = mysql_num_fields(res);

    while ((row = mysql_fetch_row(res)))
    {

        
    FILE *f;

    f = fopen("notes.txt", "a");

    if (f == NULL)
    {
        printf("Erreur lors de l'ouverture d'un fichier");
        exit(1);
    }

        moy_francais = atof(row[3]);
        moy_anglais = atof(row[4]);
        moy_math = atof(row[5]);
        moy_generale = atof(row[6]);

        printf("\n");
        printf("\n");

        printf("***********************  N°%s ***********************\n", row[0]);

        printf("\n");

        // void deliberationGenerale(struct Etudiant etudiant){
    if(moy_generale < moyReprise) {

        printf("L'etudiant : %s est exclu ! Avec une moyenne de : %.2f !\n", row[2], moy_generale);
        printf("********* ANGLAIS *********\n");
        deliberationMatiere(moy_anglais);
        printf("\n");
        printf("********* FRANCAIS *********\n");
        deliberationMatiere(moy_francais);
        printf("\n");
        printf("********* MATHEMATIQUE *********\n");
        deliberationMatiere(moy_math);
        printf("\n");

        fprintf(f, "%s est exclu ! Avec une moyenne de : %.2f !\n", row[2], moy_generale);
        fclose(f);

    } else if ((moy_generale > moyReprise) && (moy_generale <= moyReprise)) {
        printf("%s reprend sa classe avec une moyenne de %.2f !\n", row[2], moy_generale);
        fprintf(f, "%s est exclu ! Avec une moyenne de : %.2f !\n", row[2], moy_generale);
        fclose(f);
    } 
    else if (moy_generale >= moyAdmin)
    {
        printf("%s est admin avec une moyenne de : %.2f !\n", row[2], moy_generale);
        fprintf(f, "%s est admin avec une moyenne de : %.2f !\n", row[2], moy_generale);
        fclose(f);
    }

        printf("\n");
    }


    // Ecriture dans le fichier

    mysql_free_result(res);
    mysql_close(conn);
}


void classement(){

    static char *host = "localhost";
    static char *user = "root";
    static char *pass = "*23Juillet1998";
    static char *dbname = "gestion_note";

    unsigned int port = 3306;
    static char *unix_socket = NULL;
    unsigned int flag = 0;
    

    char requete[2000] = "select nom_prenom,matricule,moy_generale from etudiant_note order by moy_generale desc";

    int i = 0;
    int num_fields;

    MYSQL *conn;
    MYSQL_RES *res;
    MYSQL_ROW row;

    conn = mysql_init(NULL);

    if(!(mysql_real_connect(conn, host, user, pass, dbname, port, unix_socket, flag))){

        fprintf(stderr, "\nError: %s [%d]\n", mysql_error(conn), mysql_errno(conn));
        exit(1);

    }
    //printf("Connection Successful! \n\n");

    //sprintf(requete, "SELECT * FROM vehicule WHERE date_sortie_vehicule = NULL");

    mysql_query(conn, requete);

    res = mysql_store_result(conn);

    num_fields = mysql_num_fields(res);

    printf("NOM ET PRENOM | MATRICULE | MOYENNE GENERALE\n");
    while ((row = mysql_fetch_row(res)))
    {

        printf("%s | %s | %s\n", row[0],row[1],row[2]);
    
    }


    // Ecriture dans le fichier

    mysql_free_result(res);
    mysql_close(conn);
}