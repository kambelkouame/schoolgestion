#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>

#include "gestNote.h"
#include "lectureSaisi.h"

void initialisation(struct Etudiant *etudiants, int taille)
{
    for (int i = 0; i < taille; i++)
    {
        strcpy(etudiants[i].matricule, "");
        strcpy(etudiants[i].nomEtPrenom, "");
        etudiants[i].math.devoir = 0;
        etudiants[i].math.examen = 0;
        etudiants[i].math.note[1] = 0;
        etudiants[i].math.note[2] = 0;
    }
}

float calculMoyenneMatiere(struct Matiere matiere)
{
    float moy = 0;

    moy = ((matiere.devoir * coefDevoir) + matiere.note[1] + matiere.note[2] + (matiere.examen * coefExamen)) / (2 + coefDevoir + coefExamen);

    return moy;
}

float calculMoyenneEtudiant(struct Etudiant etudiant)
{
    float moy = 0;

    moy = ((calculMoyenneMatiere(etudiant.francais) * coefFrancais) + (calculMoyenneMatiere(etudiant.anglais) * coefAnglais) + (calculMoyenneMatiere(etudiant.math) * coefMath)) / (coefAnglais + coefFrancais + coefMath);

    return moy;
}

void EnregistrementEtudiant(struct Etudiant *etudiants, int i)
{

    

    printf("Etudiant : %d\n", i + 1);
    printf("Veuillez entrer le matricule : ");
    lire(etudiants[i].matricule, 10);

    printf("Veuillez entrer le nom et le(s) prenom(s) de l'etudiant : ");
    lire(etudiants[i].nomEtPrenom, 100);

    printf("**************************** Notes ****************************\n");
    printf("---------------- Notes en francais ----------------\n");
    printf("Note 1 : ");
    scanf("%f", &etudiants[i].francais.note[1]);
    printf("Note 2 : ");
    scanf("%f", &etudiants[i].francais.note[2]);
    printf("Note de devoir : ");
    scanf("%f", &etudiants[i].francais.devoir);
    printf("Note d'examen : ");
    scanf("%f", &etudiants[i].francais.examen);

    printf("---------------- Notes en mathematique ----------------\n");
    printf("Note 1 : ");
    scanf("%f", &etudiants[i].math.note[1]);
    printf("Note 2 : ");
    scanf("%f", &etudiants[i].math.note[2]);
    printf("Note de devoir : ");
    scanf("%f", &etudiants[i].math.devoir);
    printf("Note d'examen : ");
    scanf("%f", &etudiants[i].math.examen);

    printf("---------------- Notes en anglais ----------------\n");
    printf("Note 1 : ");
    scanf("%f", &etudiants[i].anglais.note[1]);
    printf("Note 2 : ");
    scanf("%f", &etudiants[i].anglais.note[2]);
    printf("Note de devoir : ");
    scanf("%f", &etudiants[i].anglais.devoir);
    printf("Note d'examen : ");
    scanf("%f", &etudiants[i].anglais.examen);

    
    

    viderBuffer();
}

void saveData(struct Etudiant etudiant)
{

    static char *host = "localhost";
    static char *user = "root";
    static char *pass = "*23Juillet1998";
    static char *dbname = "gestion_note";

    unsigned int port = 3306;
    static char *unix_socket = NULL;
    unsigned int flag = 0;

     char requete[2000];

    MYSQL *conn;
    MYSQL_RES *res;
    MYSQL_ROW row;

    conn = mysql_init(NULL);

    if(!(mysql_real_connect(conn, host, user, pass, dbname, port, unix_socket, flag))){

        fprintf(stderr, "\nError: %s [%d]\n", mysql_error(conn), mysql_errno(conn));
        exit(1);

    }

    printf("Connection Successful! \n\n");


    // FILE *f;

    // f = fopen("notes.txt", "a");

    // if (f == NULL)
    // {
    //     printf("Erreur lors de l'ouverture d'un fichier");
    //     exit(1);
    // }


    // fprintf(f, "%s %s Francais : %.2f Anglais : %.2f Mathematique : %.2f Moyenne generale : %.2f %s\n", etudiant.matricule, etudiant.nomEtPrenom, calculMoyenneMatiere(etudiant.francais), calculMoyenneMatiere(etudiant.anglais), calculMoyenneMatiere(etudiant.math), calculMoyenneEtudiant(etudiant), __DATE__);

    // fclose(f);

    sprintf(requete, "insert INTO etudiant_note (matricule,nom_prenom,moy_francais,moy_anglais,moy_math,moy_generale,date)VALUES('%s','%s','%.2f','%.2f','%.2f','%.2f',NOW());", etudiant.matricule, etudiant.nomEtPrenom, calculMoyenneMatiere(etudiant.francais), calculMoyenneMatiere(etudiant.anglais), calculMoyenneMatiere(etudiant.math), calculMoyenneEtudiant(etudiant));

    viderBuffer();
    
    printf("%s", requete);
  // mysql_free_result(res);
   if( (mysql_query(conn, requete)) == 0){
       printf("insertion reussite...");
   } else {
       printf("insertion à echouer...");
   }
    mysql_close(conn);
}

void affichierFichier()
{
    FILE *f;
    int lettre = 0;

    f = fopen("notes.txt", "r");

    while ((lettre = fgetc(f)) != EOF)
    {
        printf("%c", lettre);
    }

    fclose(f);
}
/*
void ticket_entree(){

    static char *host = "localhost";
    static char *user = "meless";
    static char *pass = "*23Juillet1998";
    static char *dbname = "parking";

    unsigned int port = 3306;
    static char *unix_socket = NULL;
    unsigned int flag = 0;

    

    char* requete = "SELECT * FROM vehicule ORDER BY id DESC LIMIT 1";

    int i = 0;
    int num_fields;

    MYSQL *conn;
    MYSQL_RES *res;
    MYSQL_ROW row;

    conn = mysql_init(NULL);

    if(!(mysql_real_connect(conn, host, user, pass, dbname, port, unix_socket, flag))){

        fprintf(stderr, "\nError: %s [%d]\n", mysql_error(conn), mysql_errno(conn));
        exit(1);

    }
    //printf("Connection Successful! \n\n");

    //sprintf(requete, "SELECT * FROM vehicule WHERE date_sortie_vehicule = NULL");

    mysql_query(conn, requete);

    res = mysql_store_result(conn);

    num_fields = mysql_num_fields(res);

    while ((row = mysql_fetch_row(res)))
    {
        printf("\n");
        printf("\n");

        printf("*********************** TICKET D'ENTREE N°%s ***********************\n", row[0]);

        printf("\n");

        printf("\t    MATRICULE DU VEHICULE ------> %s\n", row[2]);

        printf("\t    MARQUE DU VEHICULE ---------> %s\n", row[1]);

        printf("\t    COULEUR DE LA VEHICULE -----> %s\n", row[3]);

        printf("\t    CATEGORIE DE LA VEHICULE ---> %s\n", row[5]);

        printf("\t    HEURE ARRIVEE DU VEHICULE --> %s\n", row[6]);

        printf("\n");
        printf("\n");

    }

    mysql_free_result(res);
    mysql_close(conn);

}
*/