#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "lectureSaisi.h"
// #include "gestNote.h"
#include "Deliberation.h"

int main(int argc, char *argv[])
{
    int tailleTab = 5, choix;

    struct Etudiant etudiants[tailleTab];

    initialisation(etudiants, tailleTab);
    do
    {
        printf("Bienvenu, veuillez choisir l'operation a effectuer !\n1- Enregistrer les etudiants et leurs notes\n2- Deliberation\n3- Classement\n4- Quitter\n");
        scanf("%d", &choix);

        switch (choix)
        {
        case 1:
            viderBuffer();
            printf("******************* Enregistrer les etudiants et leurs notes *******************\n");

            for (int i = 0; i < tailleTab; i++)
            {
                EnregistrementEtudiant(etudiants, i);

                saveData(etudiants[i]);

                printf("Enregistrement effectue !\n");
                printf("\n");
                printf("\n");
                printf("\n");
            }

            break;
        case 2:

            deliberationGenerale();
                printf("\n");

            break;
        case 3:
            classement();
            printf("\n");
            
            break;
        case 4:
            printf("Merci !\n");

            break;    
        default:
            printf("Choix non disponible !\n");

            #if(choix > 3)
                printf("Veuillez faire un choix parmis ceux que nous proposons !\n");
            #endif

            break;
        }

    } while (choix != 3);

    return 0;
}
