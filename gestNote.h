
#define coefDevoir 2
#define coefExamen 2
#define coefFrancais 1
#define coefAnglais 1
#define coefMath  1
#define moyReprise 8
#define moyAdmin 10

typedef struct Matiere
{
    float note[2];
    float devoir;
    float examen;
}Matiere;

typedef struct Etudiant
{
    char matricule[10];
    char nomEtPrenom[100];
    struct Matiere francais;
    struct Matiere anglais;
    struct Matiere math;
}Etudiant;


void initialisation(struct Etudiant *etudiants, int taille);
float calculMoyenneMatiere(struct Matiere matiere);
float calculMoyenneEtudiant(struct Etudiant etudiant);
void EnregistrementEtudiant(struct Etudiant *etudiants, int i);
void saveData(struct Etudiant etudiant);
void affichierFichier();